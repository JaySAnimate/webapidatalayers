﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIDataToViewToData.Models
{
    public class MovieInfo
    {
        [Required(ErrorMessage = "Required*")]
        public int MovieId { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string MovieName { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Director { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Ganre { get; set; }
    }
}