﻿using System.Web.Mvc;
using WebAPIDataToViewToData.Models;
using BusinessLogic;

namespace WebAPIDataToViewToData.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult RegisterMovie()
        {
            ViewBag.Message = "Register Movie details";

            return View();
        }

        [HttpPost]
        public ActionResult RegisterMovie(MovieInfo infoModel)
        {
            ViewBag.Message = "Register Movie details";

            DataBusinessLogic.SaveNewData(infoModel.MovieId,
                                          infoModel.MovieName,
                                          infoModel.Director,
                                          infoModel.Ganre);
            return View();
        }

    }
}
