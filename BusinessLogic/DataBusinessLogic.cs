﻿using System.Collections;
using BusinessLogic.Models;
using MovieDataAccess;

namespace BusinessLogic
{
    public static class DataBusinessLogic
    {
        public static void SaveNewData(int movieId, string movieName, string director, string ganre)
        {
            DbModel data = new DbModel()
            {
                MovieId = movieId,
                MovieName = movieName,
                Director = director,
                Ganre = ganre
            };

            string sqlCommand = @"insert into dbo.Movie (MovieId, MovieName, Director, Ganre)
                                  values (@MovieId, @MovieName, @Director, @Ganre);";    

            MDataAccess.SaveData(data, sqlCommand);
        }
        
        public static IEnumerable GetData()
        {
            string sqlCommand = @"select MovieId, MovieName, Director, Ganre
                                  from dbo.Movie;";

            return MDataAccess.GetData(sqlCommand);
        }
    }
}
