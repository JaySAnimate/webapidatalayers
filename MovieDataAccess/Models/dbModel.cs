﻿namespace BusinessLogic.Models
{
    public class DbModel
    {
        public int MovieId { get; set; }
        public string MovieName { get; set; }
        public string Director { get; set; }
        public string Ganre { get; set; }
    }
}
