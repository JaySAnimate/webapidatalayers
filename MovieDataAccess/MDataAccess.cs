﻿using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BusinessLogic.Models;
using Dapper;

namespace MovieDataAccess
{
    public static class MDataAccess
    {
        public static string GetConnectionStr(string ConnName) => ConfigurationManager.ConnectionStrings[ConnName].ConnectionString;

        public static IEnumerable GetData(string sqlCommand)
        {
            IDbConnection dbConnection = new SqlConnection(GetConnectionStr("MovieDB"));
            return dbConnection.Query(sqlCommand);
        }

        public static int SaveData(DbModel data, string sqlCommand)
        {
            IDbConnection dbConnection = new SqlConnection(GetConnectionStr("MovieDB"));
            return dbConnection.Execute(sqlCommand, data);
        }
    }
}