﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonData
{
    public class ListOfData
    {
        static ListOfData()
        {
            _movieData = new List<MovieInfo>();
        }

        public static List<MovieInfo> _movieData;
    }
}
