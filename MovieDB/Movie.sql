﻿CREATE TABLE [dbo].[Movie]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [MovieId] INT NOT NULL, 
    [MovieName] NVARCHAR(50) NOT NULL, 
    [Director] NVARCHAR(50) NOT NULL, 
    [Ganre] NVARCHAR(50) NOT NULL
)